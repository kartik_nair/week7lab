#include <iostream>
#include <new>
#include <string>

struct Employee {
    std::string first_name, last_name, email, phone_number;
    double id;
    double salary;
    Employee* manager;
};

void print_employee(Employee& e) {
    std::cout << "\n\tName: "
              << e.first_name
              << " "
              << e.last_name
              << "\n\tID: "
              << e.id
              << "\n\teMail: "
              << e.email
              << "\n\tPhone Number: "
              << e.phone_number
              << "\n\tSalary: "
              << e.salary;

    if (e.manager != nullptr) {
        std::cout << "\n\tManager Name: "
                  << e.manager->first_name
		  << " "
                  << e.manager->last_name;
    }

    std::cout << std::endl;
}


std::string prompt_string(const char* question) {
    std::string value;
    std::cout << question;
    std::getline(std::cin >> std::ws, value);
    return value;
}

double prompt_double(const char* question) {
    double value;
    std::cout << question;
    std::cin >> value;
    return value;
}

int main() {
    int number_of_employees;
    Employee* employees;

    number_of_employees =
      prompt_double("Please enter the number of employees you would like: ");
    employees = new Employee[number_of_employees];

    for (int i = 0; i < number_of_employees; i++) {
        employees[i].id = i;
        employees[i].manager = nullptr;

        employees[i].first_name = prompt_string("\nPlease enter employees first name: ");
        employees[i].last_name = prompt_string("Please enter employees last name: ");
        employees[i].email = prompt_string("Please enter employees email: ");
        employees[i].phone_number = prompt_string("Please enter employees phone number: ");
        employees[i].salary = prompt_double("Please enter employees salary: ");
    }

    std::cout << "\nYour employees: " << std::endl;

    for (int i = 0; i < number_of_employees; i++) {
        print_employee(employees[i]);
    }

    // clean up
    delete[] employees;
}
