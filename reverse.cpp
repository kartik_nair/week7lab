#include <iostream>
#include <cstring>

void reverse(char s[]) {
    size_t start = 0,
           length = strlen(s) - 1;
    char* p = s;

    while (start < length) {
        std::swap(s[start], s[length]);
        start++;
        length--;
    }
}

/* Small test program */
int main() {
    char test[] = "Harry";
    std::cout << "Initial char array: " << test << std::endl;
    reverse(test);
    std::cout << "Reversed array: " << test << std::endl;
}
